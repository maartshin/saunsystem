------------------------------------------------------------------------------
--  File: behavioural.vhd
--behavioural model of SaunSystem
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity SaunSystemBeh is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_BEH  : out std_logic_vector(3 downto 0));
end SaunSystemBeh;
  
architecture behave of SaunSystemBeh is

begin
	--behavioural model of SaunSystem
	process (A_IN, B_IN, C_IN, D_IN, E_IN, F_IN) begin
		if E_IN = '1' then
			OUT_SIG_BEH(3) <= '1'; --y1 output
		elsif E_IN = '0' then
			OUT_SIG_BEH(3) <= '0'; --y1 output
		else
			OUT_SIG_BEH(3) <= 'X'; --y1 output
		end if;
		if (A_IN = '1' and B_IN = '0' and E_IN = '0') then
			OUT_SIG_BEH(2) <= '1'; --y2 output
		elsif (A_IN = '0' or B_IN = '1' or E_IN = '1') then
			OUT_SIG_BEH(2) <= '0'; --y2 output
		else
			OUT_SIG_BEH(2) <= 'X'; --y2 output
		end if;
		if (C_IN = '0' and D_IN = '1' and E_IN = '0') then
			OUT_SIG_BEH(1) <= '0'; --y3 output
		elsif (C_IN = '1' or D_IN = '0' or E_IN = '1') then
			OUT_SIG_BEH(1) <= '1'; --y3 output
		else
			OUT_SIG_BEH(1) <= 'X'; --y3 output
		end if;
		if ((B_IN = '1' and F_IN = '0') or (A_IN = '0' and F_IN = '0')) then
			OUT_SIG_BEH(0) <= '0'; --y4 output
		elsif ((B_IN = '0' or F_IN = '1') and (A_IN = '1' or F_IN = '1')) then
			OUT_SIG_BEH(0) <= '1'; --y4 output
		else
			OUT_SIG_BEH(0) <= 'X'; --y4 output
		end if;
	end process;

end behave;