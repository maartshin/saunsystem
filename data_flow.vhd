------------------------------------------------------------------------------
--  File: data_flow.vhd
--dataflow of SaunSystem
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

--SaunSystemFlow entity
 entity SaunSystemFlow is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_FLOW  : out std_logic_vector(3 downto 0));
  end SaunSystemFlow;
  
--Architecture of the SaunSystemFlow
architecture dataflow of SaunSystemFlow is

begin

out_sig_flow(3) <= E_IN; --y1 output
out_sig_flow(2) <= A_IN and (not B_IN) and (not E_IN); --y2 output
out_sig_flow(1) <= not((not C_IN) and D_IN and (not E_IN)); --y3 output
out_sig_flow(0) <= not((B_IN and (not F_IN)) or ((not A_IN) and (not F_IN))); --y4 output

end dataflow;