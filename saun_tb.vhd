------------------------------------------------------------------------------
--  File: mux_tb.vhd
------------------------------------------------------------------------------
--Multiplexor testbench
library IEEE;
use IEEE.std_logic_1164.all;

--Testbench entity is always empty
entity SaunSystemTB is
end SaunSystemTB;

architecture Bench of SaunSystemTB is

  --Component declaration for MUX
  component SaunSystem is
  port ( inputvect :in std_logic_vector(5 downto 0);
         OUT_SIG_BEH  : out std_logic_vector(3 downto 0);
         OUT_SIG_FLOW  : out std_logic_vector(3 downto 0);
         OUT_SIG_STRUCT  : out std_logic_vector(3 downto 0);
         OUT_SIG_TT : out std_logic_vector(3 downto 0));
  end component;

  --Local signal declarations
  signal inputvect_TB : std_logic_vector(5 downto 0); 
  signal OutSigBehTB, OutSigFlowTB, OutSigStructTB, OutSigTT_TB : std_logic_vector(3 downto 0);

begin

--Component instantiation of SaunSystem
SaunSystem_comp: SaunSystem port map (inputvect_TB, OutSigBehTB, OutSigFlowTB, OutSigStructTB, OutSigTT_TB);
 
--Stimulus process
Stimulus: process
   begin
      inputvect_TB<="011111";
      wait for 10 ns;
	  --assert OutSigBehTB = BInTB report "OutSigBehTB /= inputvect_TB(5)" severity note; --The assert statement tests the boolean condition. If this is false, it outputs a message containing the report string to the simulator screen.
	  
      inputvect_TB<="011010";
      wait for 10 ns;

      inputvect_TB<="000000";
      wait for 10 ns;
      
      inputvect_TB<="111111";
      wait for 10 ns;
      
      inputvect_TB<="111010";
      wait for 10 ns;
      
      

      wait;  --Suspend
   end process Stimulus;

end Bench;