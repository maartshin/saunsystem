------------------------------------------------------------------------------
--  File: 3-and_gate.vhd
--and gate with 3 inputs
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;


entity and_gate3 is
port( a:in std_logic;
      b:in std_logic;
      c:in std_logic;
      o:out std_logic);
end and_gate3;

architecture dataflow_and of and_gate3 is
begin
o<=a and b and c;
end dataflow_and;