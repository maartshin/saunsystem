------------------------------------------------------------------------------
--  File: or_gate.vhd
--or gate
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;


entity or_gate is
port( a:in std_logic;
      b:in std_logic;
      o:out std_logic);
end or_gate;

architecture dataflow_or of or_gate is
begin
o<=a or b;
end dataflow_or;