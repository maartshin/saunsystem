------------------------------------------------------------------------------
--  File: 2-and_gate.vhd
--and gate with 2 inputs
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

entity and_gate is
port( a:in std_logic;
      b:in std_logic;
      o:out std_logic);
end and_gate;

architecture dataflow_and of and_gate is
begin
o<=a and b;
end dataflow_and;