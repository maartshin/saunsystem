------------------------------------------------------------------------------
--  File: not_gate.vhd
--not gate
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;


entity not_gate is
port( a:in std_logic;
      o:out std_logic;);
end not_gate;

architecture dataflow_not of not_gate is
begin
o<=not(a);
end dataflow_not;