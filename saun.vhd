------------------------------------------------------------------------------
--  File: saun.vhd
--
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

--SaunSystem entity
 entity SaunSystem is
  port ( 
         inputvect :in std_logic_vector(5 downto 0);
         OUT_SIG_BEH  : out std_logic_vector(3 downto 0);
         OUT_SIG_FLOW  : out std_logic_vector(3 downto 0);
         OUT_SIG_STRUCT  : out std_logic_vector(3 downto 0);
         OUT_SIG_TT : out std_logic_vector(3 downto 0));
  end SaunSystem;
  
--Architecture of the SaunSystem
architecture RTL of SaunSystem is

--Structural model of Saunsystem
component SaunSystemStruct is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_STRUCT  : out std_logic_vector(3 downto 0));
  end component;

--Dataflow of Saunsystem
component SaunSystemFlow is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_FLOW  : out std_logic_vector(3 downto 0));
  end component;

--Behavioural model of SaunSystem
component SaunSystemBeh is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_BEH : out std_logic_vector(3 downto 0));
  end component;
  
  --truth table for SaunSystem
  component SaunSystemTT is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_TT  : out std_logic_vector(3 downto 0));
  end component;
  
  signal A_IN,B_IN,C_IN,D_IN,E_IN,F_IN:std_logic;
  
  
begin

A_IN <=inputvect(5);
B_IN <=inputvect(4);
C_IN <=inputvect(3);
D_IN <=inputvect(2);
E_IN <=inputvect(1);
F_IN <=inputvect(0);

--Truth table, structural, dataflow and behavioural output of SaunSystem.
A1:SaunSystemStruct port map (A_IN ,B_IN ,C_IN ,D_IN ,E_IN ,F_IN, OUT_SIG_STRUCT);
A2:SaunSystemFlow port map (A_IN ,B_IN ,C_IN ,D_IN ,E_IN ,F_IN, OUT_SIG_FLOW);
A3:SaunSystemBeh port map (A_IN ,B_IN ,C_IN ,D_IN ,E_IN ,F_IN, OUT_SIG_BEH);
A4:SaunSystemTT port map (A_IN ,B_IN ,C_IN ,D_IN ,E_IN ,F_IN, OUT_SIG_TT);



end RTL;