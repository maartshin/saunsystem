------------------------------------------------------------------------------
--  File: structural.vhd
--structural model of SaunSystem
------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

--SaunSystemStruct entity
 entity SaunSystemStruct is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_STRUCT  : out std_logic_vector(3 downto 0));
  end SaunSystemStruct;
  
--Architecture of the SaunSystemStruct
architecture structural of SaunSystemStruct is

--not gate
component not_gate is
port( a:in std_logic;
      o:out std_logic;);
end component;

--or gate
component or_gate is
port( a:in std_logic;
      b:in std_logic;
      o:out std_logic);
end component;

--and gate
component and_gate is
port( a:in std_logic;
      b:in std_logic;
      o:out std_logic);
end component;

--and gate with 3 inputs
component and_gate3 is
port( a:in std_logic;
      b:in std_logic;
      c:in std_logic;
      o:out std_logic);
end component;

signal not_f_in, not_e_in, not_c_in,not_b_in, not_a_in:std_logic:='U'; --inversions of a_in,b_in,c_in,e_in and f_in
signal not_y4_out, not_y3_out :std_logic:='U';--inversions of y3 and y4 output
signal y4_1_con, y4_2_con :std_logic:='U'; --conjuctions in y4

begin

G1:not_gate port map (f_in,not_f_in); --inversion of f_in
G2:not_gate port map (e_in,not_e_in); --inversion of e_in
G3:not_gate port map (c_in,not_c_in); --inversion of c_in
G4:not_gate port map (b_in,not_b_in); --inversion of b_in
G5:not_gate port map (a_in,not_a_in); --inversion of a_in
G6:not_gate port map (not_e_in,OUT_SIG_STRUCT(3)); --y1 output
G7:and_gate3 port map (a_in,not_b_in,not_e_in,OUT_SIG_STRUCT(2)); --y2 output
G8:and_gate3 port map (not_c_in,d_in,not_e_in,not_y3_out); -- inversion of y3 output
G9:not_gate port map (not_y3_out, OUT_SIG_STRUCT(1)); --y3 output
G10:and_gate port map(b_in,not_f_in,y4_1_con);--first implicant in y4 output
G11:and_gate port map(not_a_in,not_f_in,y4_2_con); --second implicant in y4 output
G12:or_gate port map(y4_1_con,y4_2_con,not_y4_out); --inversion of y4 output
G13:not_gate port map(not_y4_out,OUT_SIG_STRUCT(0)); -- y4 output

end structural;