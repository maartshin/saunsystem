library IEEE;
use IEEE.std_logic_1164.all;

--SaunSystemFlow entity
 entity SaunSystemTT is
  port ( A_IN     : in std_logic;
         B_IN     : in std_logic;
         C_IN     : in std_logic;
         D_IN     : in std_logic;
         E_IN     : in std_logic;
         F_IN     : in std_logic;
         OUT_SIG_TT  : out std_logic_vector(3 downto 0));
  end SaunSystemTT;


architecture tt of SaunSystemTT is

	signal inputs : STD_LOGIC_VECTOR(5 downto 0);--x1,...,x6
	--truth table
begin
	
  inputs <= A_IN & B_IN & C_IN & D_IN & E_IN & F_IN;
  
  WITH inputs SELECT 
	OUT_SIG_TT <= "0010" WHEN "000000",
			     "0011" WHEN "000001",
			     "1010" WHEN "000010",
			     "1011" WHEN "000011",
			     "0000" WHEN "000100",
			     "0001" WHEN "000101",
			     "1010" WHEN "000110",
			     "1011" WHEN "000111",
			     "0010" WHEN "001000",
			     "0011" WHEN "001001",
			     "1010" WHEN "001010",
			     "1011" WHEN "001011",
			     "0010" WHEN "001100",
			     "0011" WHEN "001101",
			     "1010" WHEN "001110",
			     "1011" WHEN "001111",
			     "0010" WHEN "010000",
			     "0011" WHEN "010001",
			     "1010" WHEN "010010",
			     "1011" WHEN "010011",
			     "0000" WHEN "010100",
			     "0001" WHEN "010101",
			     "1010" WHEN "010110",
			     "1011" WHEN "010111",
			     "0010" WHEN "011000",
			     "0011" WHEN "011001",
			     "1010" WHEN "011010",
			     "1011" WHEN "011011",
			     "0010" WHEN "011100",
			     "0011" WHEN "011101",
			     "1010" WHEN "011110",
			     "1011" WHEN "011111",
			     "0111" WHEN "100000",
			     "0111" WHEN "100001",
			     "1011" WHEN "100010",
			     "1011" WHEN "100011",
			     "0101" WHEN "100100",
			     "0101" WHEN "100101",
			     "1011" WHEN "100110",
			     "1011" WHEN "100111",
			     "0111" WHEN "101000",
			     "0111" WHEN "101001",
			     "1011" WHEN "101010",
			     "1011" WHEN "101011",
			     "0111" WHEN "101100",
			     "0111" WHEN "101101",
			     "1011" WHEN "101110",
			     "1011" WHEN "101111",
			     "0010" WHEN "110000",
			     "0011" WHEN "110001",
			     "1010" WHEN "110010",
			     "1011" WHEN "110011",
			     "0000" WHEN "110100",
			     "0001" WHEN "110101",
			     "1010" WHEN "110110",
			     "1011" WHEN "110111",
			     "0010" WHEN "111000",
			     "0011" WHEN "111001",
			     "1010" WHEN "111010",
			     "1011" WHEN "111011",
			     "0010" WHEN "111100",
			     "0011" WHEN "111101",
			     "1010" WHEN "111110",
			     "1011" WHEN "111111";
end tt;
